﻿using UnityEngine;
using System.Collections;
using Managers;
using UnityEngine.UI;

namespace gc {
    public class FlowerScript : MonoBehaviour {

        //public Color flowerColor;

        private SpriteRenderer _spriteRenderer;

        private Pollen _startingPollen;
        public Pollen CurrentPollen;

        private float spawnTime;
        public float MinSpawnTime;
        public float MaxSpawnTime;
        public float distanceMultiplier = 1;

        private float pollenRegenAmount = .25f;

        private float timePassed;

        public bool BeeVisited { get; set; }

        public HSVColor flowerColor;

        void Awake()
        {
            timePassed = 0;
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }


        void Start() {
            
            spawnTime = Random.Range(MinSpawnTime, MaxSpawnTime);
        }

        void Update()
        {
            if (FlowerManager.Instance()._isPaused || !BeeVisited)
            {
                return;
            }

            timePassed += Time.deltaTime;
            if (timePassed > spawnTime)
            {
                timePassed = 0;
                spawnTime = Random.Range(MinSpawnTime, MaxSpawnTime);
                SpawnPlant();
            }
        }

        public void Init(HSVColor newflowerColor)
        {
            flowerColor = newflowerColor;
            _spriteRenderer.color = flowerColor.RGBColor;
        }

        void SpawnPlant()
        {
            Vector2 randomPosition = (Vector2)transform.position + (Random.insideUnitCircle * distanceMultiplier);
            GameObject newFlower = Instantiate(FlowerManager.Instance().FlowerResource, randomPosition, Quaternion.identity) as GameObject;
            newFlower.transform.parent = FlowerManager.Instance().gameObject.transform;
            
            newFlower.GetComponent<FlowerScript>().Init(flowerColor);

            BeeVisited = false;
            
            FlowerManager.Instance().FlowerList.Add(newFlower);
        }
    }
}
