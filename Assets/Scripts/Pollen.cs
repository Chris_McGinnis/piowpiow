﻿using UnityEngine;

public class Pollen
{
    public Color color;

    public float red;
    public float green;
    public float blue;

    public float h;

    private HSVColor hsvColor;

    public Pollen(Color pollenColor)
    {
        
        red = pollenColor.r;
        green = pollenColor.g;
        blue = pollenColor.b;
        color = pollenColor;
        h = pollenColor.ToHSV().h;
        hsvColor = new HSVColor(h,1,1);
    }

    public Pollen()
    {
        red = 0;
        green = 0;
        blue = 0;
        h = 1;
        hsvColor = new HSVColor(1, 1, 1);
    }

    public Color PollenColor()
    {
        hsvColor.h = h;
        return hsvColor.RGBColor;
    }

    public HSVColor MixColor(HSVColor otherColor)
    {
        return hsvColor.MixWith(otherColor);
    }

    ////public HSVColor MixWith(HSVColor otherColor)
    ////{
    ////    float new_h = AverageHSVFloats(h, otherColor.h);
    ////    float new_s = (s + otherColor.s) / 2;
    ////    float new_v = (v + otherColor.v) / 2;
    ////    return new HSVColor(new_h, new_s, new_v);
    ////}

    //private float AverageHSVFloats(float x, float y)
    //{
    //    // x = (x < 0) ? (x % -1f) + 1 : x;
    //    // y = (y < 0) ? (y % -1f) + 1 : y;
    //    x = ClampHSVFloat(x);
    //    y = ClampHSVFloat(y);
    //    if (UnityEngine.Mathf.Abs(x - y) > 0.5f)
    //    {
    //        if (x < y)
    //        {
    //            x += 1;
    //        }
    //        else
    //        {
    //            y += 1;
    //        }
    //    }
    //    return (x + y) / 2f % 1f;
    //}

    //private float ClampHSVFloat(float x)
    //{
    //    return (x > 1f || x < 0f) ? x % 1f : x;
    //}


}
