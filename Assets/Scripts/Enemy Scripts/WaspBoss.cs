﻿using gc;
using Managers;
using UnityEngine;
using UnityEngine.Assertions;

namespace Enemy
{
    public class WaspBoss : Enemy
    {

        public Vector2 position2, position3, position4;

        protected override void Start()
        {
            base.Start();
            ScaleTask scaleT = new ScaleTask(gameObject, 2.5f, .5f);
            ScaleTask scaleT2 = new ScaleTask(gameObject, 0, .5f);

            MoveTask move1 = new MoveTask(gameObject, gameObject.transform.position, position2, 4f);
            MoveTask move2 = new MoveTask(gameObject, position2, position3, 4f);
            MoveTask move3 = new MoveTask(gameObject, position3, position4, 4f);
            MoveTask move4 = new MoveTask(gameObject, position4, beeHive.transform.position, 4f);

            WaitTask wait1 = new WaitTask(1f);
            WaitTask wait2 = new WaitTask(.5f);
            WaitTask wait3 = new WaitTask(.5f);
            WaitTask wait4 = new WaitTask(.5f);
            WaitTask wait5 = new WaitTask(1f);

            SpawnWaspTask spawnWasp1 = new SpawnWaspTask(gameObject);
            SpawnWaspTask spawnWasp2 = new SpawnWaspTask(gameObject);
            SpawnWaspTask spawnWasp3 = new SpawnWaspTask(gameObject);

            DestroyTask destroyTask = new DestroyTask(gameObject);

            Services.TaskManager.AddTask(scaleT);
            scaleT.Then(move1);
            move1.Then(wait1);
            wait1.Then(spawnWasp1);
            spawnWasp1.Then(wait2);
            wait2.Then(move2);
            move2.Then(spawnWasp2);
            spawnWasp2.Then(wait3);
            wait3.Then(move3);
            move3.Then(wait4);
            wait4.Then(spawnWasp3);
            spawnWasp3.Then(wait5);
            wait5.Then(move4);
            move4.Then(scaleT2);
            scaleT2.Then(destroyTask);
        }
        


        void OnCollisionEnter2D(Collision2D coll)
        {
            if (coll.gameObject.CompareTag("PLAYER"))
            {
                Player player = coll.gameObject.GetComponent<Player>();
                Assert.IsNotNull(player, "Object tagged PLAYER but no Player script attached");
                player.SendMessage("KillPlayer");
            }
        }

    }
}