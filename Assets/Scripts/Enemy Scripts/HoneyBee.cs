﻿using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;
using StatePattern;
using gc;
using Managers;
using UnityEngine.Assertions.Comparers;

namespace Enemy
{
    public class HoneyBee : Enemy
    {
        float gatherDelay = .5f;
        float currentTime = Time.timeSinceLevelLoad;

        public float scareDistance;
        public float gatherDistance;
        public float flowerGatherDelay;

        private FSM<HoneyBee> _states;

        private GameObject targetFlower;
        private GameObject lastFlowerVisited;

        public Vector2 ScaredLoc { get; set; }

        private int flowersVisited = 0;

        private HSVColor beePollenColor;

        //Constructor
        public HoneyBee()
        {
            _states = new FSM<HoneyBee>(this);
            _states.TransitionTo<Idle>();
        }

        protected override void Start()
        {
            base.Start();
            EventManager.Instance.Register<PauseEvent>(PauseBee);
            EventManager.Instance.Register<UnPauseEvent>(UnPauseBee);
        }

        public void Update()
        {
            if (EnemyManager.Instance().isPaused) { return; }
            
            _states.Update();

            if (_states.GetType() != typeof(Fleeing))
            {
                if (DistanceToPlayer(transform.position) < scareDistance)
                {

                    ScaredLoc = transform.position;
                    _states.TransitionTo<Fleeing>();
                }
            }

        }

        #region Events

        void PauseBee(gc.Event e)
        {
            oldVelocity = body.velocity;
            body.velocity = Vector2.zero;
            body.isKinematic = true;
        }

        void UnPauseBee(gc.Event e)
        {
            body.isKinematic = false;
            body.velocity = oldVelocity;
        }

        #endregion

        #region States

        private class Idle : FSM<HoneyBee>.State
        {
            public override void Init()
            {
                //Only when things start
            }

            public override void Update()
            {
                if (Context.FindClosest(FlowerManager.Instance().FlowerList)!=null)
                {
                    
                    TransitionTo<Searching>();
                }
            }
        }

        //Looks for a flower to collect
        private class Searching : FSM<HoneyBee>.State
        {
            public override void OnEnter(FSM<HoneyBee>.State previousState)
            {

                Context.targetFlower = FlowerManager.Instance().FindRandomFlower();

                if (previousState.GetType() == typeof(Collecting))
                {
                    while(Context.targetFlower == Context.lastFlowerVisited)
                    {
                        Context.targetFlower = FlowerManager.Instance().FindRandomFlower();
                    }
                }

                Context.lastFlowerVisited = Context.targetFlower;
            }

            public override void Update()
            {
                if(Context.targetFlower !=null && FlowerManager.Instance().FlowerList.Contains(Context.targetFlower))
                {
                    Context.EnemySteering(Context.DirectionToTarget(Context.transform.position, Context.targetFlower.transform.position));

                    if (Vector2.Distance(Context.targetFlower.transform.position, Context.transform.position) < Context.gatherDistance)
                    {
                        TransitionTo<Collecting>();
                    }

                } else
                {
                    TransitionTo<Searching>();
                }

                
                
            }
        }

        //Once the bee gets to the flower location
        private class Collecting : FSM<HoneyBee>.State
        {
            float timeOnFlower;

            public override void OnEnter(FSM<HoneyBee>.State previousState)
            {
                HSVColor flowerPollenColor;

                timeOnFlower = 0;

                FlowerScript flowerScript = Context.targetFlower.GetComponent<FlowerScript>();
                flowerPollenColor = flowerScript.flowerColor;

                if (Context.flowersVisited > 0)
                {
                    HSVColor newColor = flowerPollenColor.MixWith(Context.beePollenColor);

                    Context.beePollenColor = newColor;
                    flowerScript.flowerColor = newColor;

                    flowerScript.BeeVisited = true;
                }
                else
                {
                    Context.beePollenColor = flowerPollenColor;
                } 
                
                Context.flowersVisited++;
            }

            public override void Update()
            {
                
                if (Context.targetFlower != null)
                {
                    if (Vector2.Distance(Context.targetFlower.transform.position, Context.transform.position) > Context.gatherDistance)
                    {
                        Context.EnemySteering(Context.DirectionToTarget(Context.transform.position, Context.targetFlower.transform.position));
                        
                    }
                    else
                    {
                        timeOnFlower += Time.deltaTime;
                    }

                    if (timeOnFlower > Context.flowerGatherDelay)
                    {
                        TransitionTo<Searching>();
                    }

                } else
                {
                    TransitionTo<Idle>();
                }
            }
        }

        //Returning to the hive
        private class Fleeing : FSM<HoneyBee>.State
        {
            public override void OnEnter(FSM<HoneyBee>.State previousState)
            {
                Context.ScaredLoc = Context.transform.position;
                EventManager.Instance.Fire(new AttackLocationEvent(Context.ScaredLoc));
            }

            public override void Update()
            {
                Context.EnemySteering(Context.DirectionToTarget(Context.transform.position, Context.beeHive.transform.position));

                if(Vector2.Distance(Context.transform.position, Context.beeHive.transform.position) < 1)
                {
                    TransitionTo<Destroying>();
                }
            }
        }

        //Destroys the Bee
        private class Destroying : FSM<HoneyBee>.State
        {
            public override void OnEnter(FSM<HoneyBee>.State previousState)
            {
                if (previousState.GetType() == typeof(Fleeing))
                {
                    EnemyManager.Instance().SpawnWasp(Context.beeHive.transform.position);
                    EnemyManager.Instance().DestroyHoneyBee(Context.gameObject, Context.ScaredLoc);
                    EventManager.Instance.Unregister<PauseEvent>(Context.PauseBee);
                    EventManager.Instance.Unregister<UnPauseEvent>(Context.UnPauseBee);
                }
            }
        }

        #endregion
    }
}
