﻿using gc;
using StatePattern;
using UnityEngine;
using UnityEngine.Assertions;
using Event = gc.Event;

namespace Enemy {
    public class Wasp : Enemy {

        private FSM<Wasp> _states;

        private Vector2? attackLocation;

        public float huntingPlayerDistance;
        public float searchingDistance;
        public float searchTime;

        private float totalSearchingTime;


        //Constructor
        public Wasp()
        {
            _states = new FSM<Wasp>(this);
            _states.TransitionTo<Idle>();
        }

        protected override void Start()
        {
            base.Start();
            EventManager.Instance.Register<AttackLocationEvent>(ChangeTargets);
            EventManager.Instance.Register<PauseEvent>(PauseWasp);
            EventManager.Instance.Register<UnPauseEvent>(UnPauseWasp);

            attackLocation = EnemyManager.Instance().attackTarget;
        }

        public void Update()
        {
            if (EnemyManager.Instance().isPaused)
            {
                return;
            }

            _states.Update();

            if (_states.GetType() != typeof(Attacking))
            {
                if (DistanceToPlayer(transform.position) < huntingPlayerDistance)
                {
                    _states.TransitionTo<Attacking>();

                }
            }

        }

        void OnCollisionEnter2D(Collision2D coll)
        {
            if (coll.gameObject.CompareTag("PLAYER"))
            {
                Player player = coll.gameObject.GetComponent<Player>();
                Assert.IsNotNull(player, "Object tagged PLAYER but no Player script attached");
                //Player.Instance.KillPlayer();
                player.SendMessage("KillPlayer");
            }
        }

        #region Events

        private void ChangeTargets(Event e)
        { 
            AttackLocationEvent ale = (AttackLocationEvent)e;
            attackLocation = ale.location;
        }

        void PauseWasp(Event e)
        {
            oldVelocity = body.velocity;
            body.velocity = Vector2.zero;
            body.isKinematic = true;          
        }

        void UnPauseWasp(Event e)
        {
            body.isKinematic = false;
            body.velocity = oldVelocity;
        }

        #endregion

        #region States

        //Idle state
        private class Idle : FSM<Wasp>.State
        {
            public override void Init()
            {
                //Buuzzzz
            }

            public override void Update()
            {
                if(Context.attackLocation!=null)
                {
                    TransitionTo<Hunting>();
                }


            }
        }

        //Wasp traveling to the attack location
        private class Hunting : FSM<Wasp>.State
        {
            public override void Update()
            {
                if(Context.attackLocation != null)
                {
                    Context.EnemySteering(Context.DirectionToTarget(Context.transform.position, (Vector2)Context.attackLocation));
                } else
                {
                    TransitionTo<Idle>();
                }

                if (Vector2.Distance(Context.transform.position, (Vector2)Context.attackLocation) < Context.searchingDistance)
                {
                    TransitionTo<Searching>();
                }
            }
        }

        //When the wasp is at/near the attack location.
        private class Searching : FSM<Wasp>.State
        {
            public override void Update()
            {
                Context.totalSearchingTime += Time.deltaTime;
               
                if(Context.totalSearchingTime>=Context.searchTime)
                {
                    TransitionTo<Retreating>();
                }

                if(Vector2.Distance(Context.transform.position, (Vector2)Context.attackLocation) > Context.searchingDistance)
                {
                    TransitionTo<Hunting>();
                }
            }
        }

        //Wasp chasing the player
        private class Attacking : FSM<Wasp>.State
        {
            public override void Update()
            {
                if (Player.Instance != null)
                {
                    if (Vector2.Distance(Context.transform.position, Player.Instance.transform.position) < Context.huntingPlayerDistance)
                    {
                        Context.EnemySteering(Context.DirectionToTarget(Context.transform.position, Player.Instance.transform.position));
                        if (Vector2.Distance(Context.transform.position, Player.Instance.transform.position) < .5f)
                        {
                            Player.Instance.SendMessage("KillPlayer");
                        }
                    }
                    else
                    {
                        TransitionTo<Searching>();
                    }
                } else
                {
                    TransitionTo<Searching>();
                }
            }
        }

        //Wasp returning to the beehive
        private class Retreating : FSM<Wasp>.State
        {
            public override void Update()
            {
                Context.EnemySteering(Context.DirectionToTarget(Context.transform.position, EnemyManager.Instance().beeHive.transform.position));
                
                if (Vector2.Distance(Context.transform.position, EnemyManager.Instance().beeHive.transform.position) < 1)
                {
                    TransitionTo<Destroying>();
                }
            }
        }

        //Destroys the wasp
        private class Destroying : FSM<Wasp>.State
        {
            public override void OnEnter(FSM<Wasp>.State previousState)
            {
                if (previousState.GetType() == typeof(Retreating))
                {
                    EnemyManager.Instance().DestroyWasp(Context.gameObject);
                    EventManager.Instance.Unregister<AttackLocationEvent>(Context.ChangeTargets);
                    EventManager.Instance.Unregister<PauseEvent>(Context.PauseWasp);
                    EventManager.Instance.Unregister<UnPauseEvent>(Context.UnPauseWasp);
                }
            }
        }
        
        #endregion
    }
}

