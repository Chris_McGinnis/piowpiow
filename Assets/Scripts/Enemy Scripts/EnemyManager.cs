﻿using System.Collections.Generic;
using gc;
using UnityEngine;
using Event = gc.Event;

namespace Enemy {

    public class EnemyManager : MonoBehaviour {

        private static EnemyManager instance;
        public static EnemyManager Instance()
        {
            
            if (instance != null)
            {
                return instance;         
            }
            instance = FindObjectOfType(typeof(EnemyManager)) as EnemyManager;
            return instance;
        }

        public static int NumEnemies { get; private set; }
        
        public int minNumHoneyBees = 4;

        int totalWasp;

        public List<GameObject> honeyBeeList = new List<GameObject>();
        public List<GameObject> waspList = new List<GameObject>();
        public List<GameObject> ladyBug = new List<GameObject>();
        
        public float spawnDelay = 3;
        float delayTimer;

        public GameObject beeHive;
        public Vector2 attackTarget;

        public bool isPaused;

	    // Use this for initialization
	    void Start() {
            delayTimer = 0;
            EventManager.Instance.Register<PauseEvent>(PauseGame);
            EventManager.Instance.Register<UnPauseEvent>(PauseGame);
        }

        // Update is called once per frame
        void Update()
        {
            if (isPaused)
            {
                return;
            }

            // Spawn enemies
            if ((honeyBeeList.Count < minNumHoneyBees) && (delayTimer >= spawnDelay))
            {
                SpawnHoneyBee();
                delayTimer = 0;
            }

            delayTimer += Time.deltaTime;

        }


        /// <summary>
        /// Spawns a new honeyBee at the beeHive
        /// </summary>
        public void SpawnHoneyBee()
        {
            GameObject newEnemy = Spawner.Spawn("Prefabs/Enemies/HoneyBee", beeHive.transform.position);
            honeyBeeList.Add(newEnemy);
            newEnemy.transform.parent = gameObject.transform;
        }

        /// <summary>
        /// Spawns a new wasp at the beeHive Location
        /// </summary>
        public void SpawnWasp(Vector2 spawnLocation)
        {
            
            if (totalWasp % 8f == 0 && totalWasp !=0)
            {
                GameObject newWasp = Spawner.Spawn("Prefabs/Enemies/BossWasp", spawnLocation);
                waspList.Add(newWasp);
            }
            else {
                GameObject newWasp = Spawner.Spawn("Prefabs/Enemies/Wasp", spawnLocation);
                waspList.Add(newWasp);
                newWasp.transform.parent = gameObject.transform;
                totalWasp++;
            }       
        }

        /// <summary>
        /// Destroys the passed bee and removes from the list. Changes to a new attack target.
        /// </summary>
        /// <param name="bee"></param>
        /// <param name="attTar"></param>
        public void DestroyHoneyBee(GameObject bee, Vector2 attTar)
        {
            delayTimer = Time.timeSinceLevelLoad;
            attackTarget = attTar;
            honeyBeeList.Remove(bee);
            Destroy(bee);
        }

        /// <summary>
        /// Destroys the wasp and removes it from the list
        /// </summary>
        /// <param name="enemy"></param>
        public void DestroyWasp(GameObject enemy)
        {
            waspList.Remove(enemy);
            Destroy(enemy);
        }

        private Vector3 RandomWorldPosition(int gutter)
        {
            return new Vector3(Random.Range(gutter, Config.WorldWidth - gutter), Random.Range(gutter, Config.WorldHeight - gutter), 0);
        }


        void PauseGame(Event e)
        {
            if(!isPaused)
            {
                isPaused = true;
            }
            else
            {
                isPaused = false;
            }
        }

    }
}