﻿using StatePattern;
using UnityEngine;

namespace Enemy
{
    public class LadyBug : Enemy
    {
        public float beeHuntingDistance;
        public float searchingTime;

        private FSM<LadyBug> _states;
        
        public LadyBug()
        {
            _states = new FSM<LadyBug>(this);
            _states.TransitionTo<Sleeping>();
        }

        public void Update()
        {
            _states.Update();
        }

        #region States

        private class Sleeping : FSM<LadyBug>.State
        {
            public override void Init()
            {
                //Only when things start
            }

            public override void Update()
            {
                if (Context.FindClosest(EnemyManager.Instance().honeyBeeList, Context.beeHuntingDistance) != null)
                {
                    TransitionTo<Searching>();
                }
            }
        }

        private class Searching : FSM<LadyBug>.State
        {
            float timeSearching;

            public override void OnEnter(FSM<LadyBug>.State previousState)
            {
                if(previousState.GetType() == typeof(Sleeping))
                {
                    //Make so NOOOISSE!!  BUZZZ
                }

                timeSearching = Time.time;
            }

            public override void Update()
            {
                if(Context.FindClosest(EnemyManager.Instance().honeyBeeList, Context.beeHuntingDistance))
                {
                    TransitionTo<Attacking>();
                } else if(Time.time - timeSearching > timeSearching)
                {
                    TransitionTo<Sleeping>();
                }
            }
        }

        private class Attacking : FSM<LadyBug>.State
        {
            GameObject closestBee;
            public override void Update()
            {
                closestBee = Context.FindClosest(EnemyManager.Instance().honeyBeeList, Context.beeHuntingDistance);
                if(closestBee != null)
                {
                    Context.EnemySteering(Context.DirectionToTarget(Context.transform.position, closestBee.transform.position));
                } else
                {
                    TransitionTo<Searching>();
                }
            }
        }

        private class Fleeing : FSM<LadyBug>.State
        {

        }

        #endregion
    }
}
