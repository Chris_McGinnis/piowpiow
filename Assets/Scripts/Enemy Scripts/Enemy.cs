﻿using System.Collections.Generic;
using gc;
using UnityEngine;

namespace Enemy {

	public class Enemy : MonoBehaviour {

        protected GameObject beeHive;

        public float maxSpeed;
        protected Rigidbody2D body;
        
        protected Vector2 oldVelocity;
       
		void Awake() {
            body = GetComponent<Rigidbody2D>();
        }

        protected virtual void Start()
        {
            beeHive = GameObject.FindGameObjectWithTag("BEEHIVE");
        }
        
        public GameObject FindClosest(List<GameObject> enemyList, float dist = 1000)
        {
            GameObject closest = null;
            foreach (GameObject bee in enemyList)
            {
                if (Vector2.Distance(transform.position, bee.transform.position) < dist)
                {
                    closest = bee;
                }
            }
            return closest;
        }

        public float DistanceToPlayer(Vector2 pos)
        {
            if (Player.Instance != null)
            {
                return Vector2.Distance(Player.Instance.transform.position, pos);
            }
            return Mathf.Infinity;
        }

	    #region Movement

        protected Vector2 DirectionToTarget(Vector2 myPos, Vector2 targetPos)
        {
            Vector2 offsetToTarget = targetPos - myPos;
            return offsetToTarget.normalized;
        }

        protected void EnemySteering(Vector2 movementDirection)
        {
            Vector2 desiredVelocity = movementDirection * maxSpeed;
            Vector2 steeringForce = desiredVelocity - body.velocity;
            Vector2.ClampMagnitude(steeringForce, maxSpeed);
            body.AddForce(steeringForce);
            float currentSpeed = body.velocity.magnitude;
            if (currentSpeed > maxSpeed)
            {
                body.velocity = Vector2.ClampMagnitude(body.velocity, maxSpeed);
            }

            float angle = Mathf.Rad2Deg * (Mathf.Atan2(body.velocity.y, body.velocity.x)) + 180; // The extra 180° are because the enemy assets are pointing to the left
            body.MoveRotation(angle);
        }

        protected void EnemyMoveDirection(Vector2 movementDirection)
        {
            Vector2 desiredVelocity = movementDirection * maxSpeed;
            body.AddForce(desiredVelocity);
            float currentSpeed = body.velocity.magnitude;
            if (currentSpeed > maxSpeed)
            {
                body.velocity = Vector2.ClampMagnitude(body.velocity, maxSpeed);
            }

            float angle = Mathf.Rad2Deg * (Mathf.Atan2(body.velocity.y, body.velocity.x)) + 180; // The extra 180° are because the enemy assets are pointing to the left
            body.MoveRotation(angle);
        }

        #endregion

    }

}

