﻿using UnityEngine;
using System.Collections;

public class HUEBarScript : MonoBehaviour
{

    private Vector2 lastMousePos;

    private Material currentMaterial;

	// Use this for initialization
	void Start ()
	{
	    currentMaterial = GetComponent<MeshRenderer>().sharedMaterial;
        currentMaterial.mainTextureOffset = new Vector2(0,0);
	}
	
    void OnMouseDown()
    {
        lastMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    void OnMouseDrag()
    {

        Vector2 newMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        Vector2 delta = lastMousePos - newMousePos;
        delta /= 20;

        currentMaterial.mainTextureOffset = new Vector2(currentMaterial.mainTextureOffset.x + delta.x, 0);

        lastMousePos = newMousePos;
    }
}
