﻿using Managers;
using UnityEngine;

namespace gc {

	public class Player : MonoBehaviour {

		public static Player Instance { get; private set; }

		public int Health = 100;

        public GameObject holdingFlower;

		void Awake() {
			Instance = this;
		}

        void KillPlayer()
        {
            EventManager.Instance.Fire(new PlayerDeathEvent(this));
            Destroy(gameObject);
        }

		void OnDestroy() {
			Instance = null;
		}

        void OnTriggerStay2D(Collider2D coll)
        {
            if(coll.gameObject.CompareTag("FLOWER"))
            {
                if (Input.GetMouseButtonDown(0))
                {
                    FlowerManager.Instance().CutFlower(coll.gameObject);
                }
            } else if(coll.gameObject.CompareTag("HOUSE"))
            {
                if(Input.GetMouseButtonDown(0) && holdingFlower != null)
                {
                    FlowerManager.Instance().TurnInFlower(holdingFlower);
                    holdingFlower = null;
                }

            }
        }


	}

}


