﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace gc {

	public class ResourceLoader {

        private static ResourceLoader _instance;
        public static ResourceLoader Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ResourceLoader();
                }
                return _instance;
            }
        }

        Dictionary<string, Object> cachedResources = new Dictionary<string, Object>();

		public Object GetResource(string name) {
			if (cachedResources.ContainsKey(name)) {
				return cachedResources[name];
			}
		    Object resource = Resources.Load(name);
		    Assert.IsNotNull(resource);
		    cachedResources.Add(name, resource);
		    return resource;
		}

    }

}
