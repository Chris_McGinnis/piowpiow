﻿using Enemy;
using UnityEngine;

namespace gc
{
    public class DestroyTask : Task
    {
        public DestroyTask(GameObject gameObject)
        {
            _gameObject = gameObject;
        }

        private GameObject _gameObject;

        protected override void Init()
        {
            EnemyManager.Instance().DestroyWasp(_gameObject);
            SetStatus(TaskStatus.Success);
        }

    }
}
