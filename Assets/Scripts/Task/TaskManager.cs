﻿using System;
using System.Collections.Generic;
using gc;
using UnityEngine;
using Event = gc.Event;

namespace Managers
{
    public class TaskManager : MonoBehaviour {

        bool isPaused;

        private readonly List<Task> _tasks = new List<Task>();

        void Start()
        {
            EventManager.Instance.Register<PauseEvent>(PauseTasks);
            EventManager.Instance.Register<UnPauseEvent>(PauseTasks);
        }

        private void PauseTasks(Event e)
        {
            if(!isPaused)
            {
                isPaused = true;
            } else
            {
                isPaused = false;
            }
        }
        
        public bool HasTasks()
        {
            return _tasks.Count > 0;
        }
        
        // NOTE: Only add tasks that aren't already attached
        public void AddTask(Task task)
        {
            Debug.Assert(task != null);
            Debug.Assert(!task.IsAttached);
            _tasks.Add(task);
            task.SetStatus(Task.TaskStatus.Pending);
        }
        
        public void AbortAllTasks()
        {
            for (int i = _tasks.Count - 1; i >= 0; --i)
            {
                Task t = _tasks[i];
                t.Abort();
                _tasks.RemoveAt(i);
                t.SetStatus(Task.TaskStatus.Detached);
            }
        }
        
        public void Update()
        {
            if (isPaused)
            {
                return;
            }

            for (int i = _tasks.Count - 1; i >= 0; --i)
            {
                Task task = _tasks[i];
                if (task.IsPending)
                {
                    task.SetStatus(Task.TaskStatus.Working);
                }
                if (task.IsFinished)
                {
                    HandleCompletion(task, i);
                }
                else
                {
                    task.Update();
                    if (task.IsFinished)
                    {
                        HandleCompletion(task, i);
                    }
                }
            }

        }

        private void HandleCompletion(Task t, int taskIndex)
        {
            if (t.NextTask != null && t.IsSuccessful)
            {
                AddTask(t.NextTask);
            }
            _tasks.RemoveAt(taskIndex);
            t.SetStatus(Task.TaskStatus.Detached);
        }
        
        public void AbortAll<T>() where T : Task
        {
            Type type = typeof(T);
            for (int i = _tasks.Count - 1; i >= 0; i--)
            {
                Task task = _tasks[i];
                if (task.GetType() == type)
                {
                    task.Abort();
                }
            }
        }

        public WaitTask Wait(float duration)
        {
            var task = new WaitTask(duration);
            AddTask(task);
            return task;
        }
        
        //public WaitForTask WaitFor(WaitForTask.Condition condition)
        //{
        //    var task = new WaitForTask(condition);
        //    AddTask(task);
        //    return task;
        //}
        
        public ActionTask Do(Action action)
        {
            var task = new ActionTask(action);
            AddTask(task);
            return task;
        }
    }
}