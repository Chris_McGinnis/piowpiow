﻿using UnityEngine;

namespace gc{
public class PulseScaleTask : Task {

	public PulseScaleTask(GameObject gameObject, float scaleSize, float secondsToComplete)
        {
            _startScale = gameObject.transform.localScale;
            _endScale = new Vector2(scaleSize, scaleSize);
            _gameObject = gameObject;
            _lerpSpeed = secondsToComplete;
            _lerpPercent = 0;
        }

        private Vector2 _startScale;
        private Vector2 _endScale;
        private GameObject _gameObject;

        private float _lerpPercent;
        private float _lerpSpeed;

        bool goingUp = true;
        // Update is called once per frame
        internal override void Update()
        {
            if (_lerpPercent < 1 && goingUp)
            {
                _gameObject.transform.localScale = Vector2.Lerp(_startScale, _endScale, _lerpPercent);
                _lerpPercent += Time.deltaTime / _lerpSpeed;
            } else {
                goingUp = false;
                _gameObject.transform.localScale = Vector2.Lerp(_startScale, _endScale, _lerpPercent);
                _lerpPercent -= Time.deltaTime / _lerpSpeed;
                
            }

            if (!goingUp && _lerpPercent < 0)
            {
                SetStatus(TaskStatus.Success);
            }
        }
}
}
