﻿using UnityEngine;

namespace gc
{
    public class MoveTask : Task
    {

        public MoveTask(GameObject gameObject, Vector2 startPos, Vector2 endPos, float secondsToComplete)
        {
            _startPos = startPos;
            _endPos = endPos;
            _gameObject = gameObject;
            _lerpSpeed = secondsToComplete;
            _lerpPercent = 0;
        }

        private Vector2 _startPos;
        private Vector2 _endPos;
        private GameObject _gameObject;

        private float _lerpPercent;
        private float _lerpSpeed;

        // Update is called once per frame
        internal override void Update()
        {
            if (_lerpPercent < 1)
            {
                _gameObject.transform.position = Vector2.Lerp(_startPos, _endPos, _lerpPercent);
                _lerpPercent += Time.deltaTime / _lerpSpeed;
            }
            else
            {
                SetStatus(TaskStatus.Success);
            }
        }
    }
}
