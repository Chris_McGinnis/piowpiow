﻿using UnityEngine;

namespace gc
{
    public class WaitTask : Task
    {
        private readonly float _duration;
        private float _startTime;

        public WaitTask(float duration)
        {
            _duration = duration;
        }

        protected override void Init()
        {
            _startTime = Time.time;
        }

        internal override void Update()
        {
            if (Time.time - _startTime > _duration)
            {
                SetStatus(TaskStatus.Success);
            }
        }
    }
}