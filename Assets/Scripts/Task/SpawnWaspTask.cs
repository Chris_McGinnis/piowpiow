﻿using Enemy;
using UnityEngine;

namespace gc
{
    public class SpawnWaspTask : Task
    {

        public SpawnWaspTask(GameObject spawnLocation)
        {
            _spawnLocation = spawnLocation;
        }

        private GameObject _spawnLocation;

        protected override void Init()
        {
            EnemyManager.Instance().SpawnWasp(_spawnLocation.transform.position);
            SetStatus(TaskStatus.Success);
        }
    }
}
