using UnityEngine;

public class HSVColor {
    
	public float h;
	public float s;
	public float v;

	public Color RGBColor {
		get {
			return Color.HSVToRGB(h, s, v);
		}
	}
    
	public HSVColor(float h, float s, float v)
    {
		this.h = ClampHSVFloat(h);
		this.s = ClampHSVFloat(s);
		this.v = ClampHSVFloat(v);
	}

	public HSVColor(Color color)
    {
		Color.RGBToHSV(color, out h, out s, out v);
	}

	public HSVColor MixWith(HSVColor otherColor) {
		float newH = AverageHSVFloats(h, otherColor.h);
		float newS = (s + otherColor.s) / 2;
		float newV = (v + otherColor.v) / 2;
		return new HSVColor(newH, newS, newV);
	}

	private float AverageHSVFloats(float x, float y)
    {
		x = ClampHSVFloat(x);
		y = ClampHSVFloat(y);
		if (Mathf.Abs(x - y) > 0.5f)
        {
			if (x < y) {
				x += 1;
			} else {
				y += 1;
			}
		}
		return (x + y) / 2f % 1f;
	}

	private float ClampHSVFloat(float x)
    {
		return (x > 1f || x < 0f) ? x % 1f : x;
	}

	public static HSVColor Lerp(HSVColor color1, HSVColor color2, float t)
    {
		t = Mathf.Clamp(t, 0f, 1f);
		float h1 = color1.h, h2 = color2.h;
		if (Mathf.Abs(h1 - h2) > 0.5f) {
			if (h1 < h2) {
				h1 += 1;
			}
			else {
				h2 += 1;
			}
		}
		float h = (h2 - h1) * t + h1;
		float s = (color2.s - color1.s) * t + color1.s;
		float v = (color2.v - color1.v) * t + color1.v;
		return new HSVColor(h, s, v);
	}
}
