﻿using Managers;
using UnityEngine;

namespace gc {

	public class Game : MonoBehaviour {

		public int MinNumEnemies = 4;
        GameObject home;
        Vector2 spawnPosition;

        public Color heldColor;

        private int score;
        public int Score
        {
            get; set;
        }

        public float spawnDelay;
        float respawnTimer;

        bool isPaused;

        public GameObject pausePanel;

        void Awake()
        {
            Services.TaskManager = gameObject.AddComponent<TaskManager>();
        }

        void Start()
        {
            pausePanel.SetActive(false);

            home = GameObject.FindGameObjectWithTag("HOUSE");
            spawnPosition = new Vector2(home.transform.position.x + 50, home.transform.position.y);
            //EventManager.Instance.Register<FlowerAlarm>(HeldColor);
            EventManager.Instance.Register<PlayerDeathEvent>(PlayerRespawn);
            respawnTimer = spawnDelay;
        }

		void Update () {

            if (!isPaused)
            {
                // Spawn player
                if (Player.Instance == null)
                {
                    if (respawnTimer >= spawnDelay)
                    {
                        Spawner.Spawn("Prefabs/Playership", spawnPosition);
                    }
                    respawnTimer += Time.deltaTime;
                }
            }

            if(Input.GetKeyDown(KeyCode.Escape))
            {
                if (!isPaused)
                {
                    isPaused = true;
                    EventManager.Instance.Fire(new PauseEvent());
                    pausePanel.SetActive(true);
                }
                else
                {
                    isPaused = false;
                    EventManager.Instance.Fire(new UnPauseEvent());
                    pausePanel.SetActive(false);
                }
            }
            
		}

        private void PlayerRespawn(Event e)
        {
            respawnTimer = 0;
        }

	}

}

