﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace gc
{
    public abstract class Event
    {
        public delegate void Handler(Event e);
    }

    public class EventManager
    {

        static private EventManager instance;
        static public EventManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new EventManager();
                }
                return instance;
            }
        }

        // Here is the core of the system: a dictionary that maps Type (specifically Event types in this case)
        // to Event.Handlers
        private Dictionary<Type, Event.Handler> registeredHandlers = new Dictionary<Type, Event.Handler>();

        // This is where you can add handlers for events. We use generics for 2 reasons:
        // 1. Passing around Type objects can be tedious and verbose
        // 2. Using generics allows us to add a little type safety, by getting
        // the compiler to ensure we're registering for an Event type and not
        // some other random type
        public void Register<T>(Event.Handler handler) where T : Event
        {
            // NOTE: This method does not check to see if a handler was registered twice
            Type type = typeof(T);
            if (registeredHandlers.ContainsKey(type))
            {
                registeredHandlers[type] += handler;
            }
            else
            {
                registeredHandlers[type] = handler;
            }
        }

        public void Unregister<T>(Event.Handler handler) where T : Event
        {
            Type type = typeof(T);
            Event.Handler handlers;
            if (registeredHandlers.TryGetValue(type, out handlers))
            {
                handlers -= handler;
                if (handlers == null)
                {
                    registeredHandlers.Remove(type);
                }
                else
                {
                    registeredHandlers[type] = handlers;
                }
            }
        }

        public void Fire(Event e)
        {
            Type type = e.GetType();
            Event.Handler handlers;
            if (registeredHandlers.TryGetValue(type, out handlers))
            {
                handlers(e);
            }
        }


        List<Event> queuedEvents = new List<Event>();
        void QueueEvent(Event e)
        {
            // insert at the head of the line since the
            // queue will be processed in reverse order
            queuedEvents.Insert(0, e);

            // NOTE: To my knowledge this method makes 
            // NO guarantees regarding thread safety and you
            // should not use this with multiple threads 
        }

        void ProcessQueuedEvents()
        {
            // NOTE: processing the queue in reverse order to 
            // avoid a concurrent modification exception if 
            // an event causes another event to be queued
            // while processing the events
            for (int i = queuedEvents.Count - 1; i >= 0; --i)
            {
                Fire(queuedEvents[i]);
                queuedEvents.RemoveAt(i);
            }
        }


    }

    public class AttackLocationEvent : Event
    {
        public readonly Vector2 location;
        public AttackLocationEvent(Vector2 location)
        {
            this.location = location;
        }
    }

    public class PlayerDeathEvent : Event
    {
        public readonly Player player;
        public PlayerDeathEvent(Player player)
        {
            this.player = player;
        }
    }

    /*
    public class AttackLocationEvent : Event
    {
        public readonly HoneyBeeMovement honeyBee;
        public AttackLocationEvent(HoneyBeeMovement honeyBee)
        {
            this.honeyBee = honeyBee;
        }
    }*/

    public class PauseEvent : Event
    {
    }

    public class UnPauseEvent : Event
    {
    }
}