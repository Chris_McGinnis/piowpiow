﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using gc;
using Random = UnityEngine.Random;
using UnityEngine.UI;

namespace Managers
{
    public class FlowerManager : MonoBehaviour
    {
        
        private static FlowerManager _instance;
        public static FlowerManager Instance()
        {
            if (_instance != null)
            {
                return _instance;
            }
            else
            {
                _instance = FindObjectOfType(typeof(FlowerManager)) as FlowerManager;
                return _instance;
            }
        }

        public GameObject FlowerResource;
        public List<GameObject> FlowerList = new List<GameObject>();

        public float MinSpawnTime;
        public float MaxSpawnTime;
        private float _timePassed;
        private float _randomizedTimeTillSpawn;

        private float _startingNumber = 8;

        public float MinFlowerDistance = 5;
        
        public Transform[] FlowerLocations;

        public SpriteRenderer targetSprite;
        private HSVColor targetColor;

        public SpriteRenderer BarSpriteRenderer;

        private int _numTurnIn = 0;

        private List<GameObject> _flowerBar = new List<GameObject>();

        public bool _isPaused;

        public bool WildFlowerSpawn = false;

        //public float flowerSize;

        public Transform leftWall;
        public Transform rightWall;
        public Transform topWall;
        public Transform bottomWall;

        const float sideBufferSpawnDistance = 25f;
        const float topBufferSpawnDistance = 15f;


        private float compairBuffer = .15f;
        private HSVColor combinedColor;

        public Text ScoreText;
        private float score;

        // Use this for initialization
        private void Start()
        {
            EventManager.Instance.Register<PauseEvent>(PauseGame);
            EventManager.Instance.Register<UnPauseEvent>(PauseGame);

            for(int i = 0; i < _startingNumber; i++)
            {
                CreatePlant(SpawnRandom());
                
            }

            _randomizedTimeTillSpawn = Random.Range(MinSpawnTime, MaxSpawnTime);
            targetColor = RandomColor();
            targetSprite.color = targetColor.RGBColor;

        }

        // Update is called once per frame
        private void Update()
        {
            if (_isPaused)
            {
                return;
            }

            if (WildFlowerSpawn)
            {

                if (_timePassed > _randomizedTimeTillSpawn)
                {
                    CreatePlant(SpawnRandom());
                    _timePassed = 0;
                    _randomizedTimeTillSpawn = Random.Range(MinSpawnTime, MaxSpawnTime);
                }

                _timePassed += Time.deltaTime;

            }

        }

        private void PauseGame(gc.Event e)
        {
            if (!_isPaused)
            {
                _isPaused = true;
            }
            else
            {
                _isPaused = false;
            }
        }

        private void CreatePlant(Vector2 loc)
        {
            GameObject newFlower = (GameObject)Instantiate(FlowerResource, loc, Quaternion.identity);
            newFlower.transform.parent = gameObject.transform;

            newFlower.GetComponent<FlowerScript>().Init(RandomColor());
            FlowerList.Add(newFlower);
        }

        private Vector2 SpawnRandom()
        {
            Vector2 randomVec;
            randomVec.x = Random.Range(leftWall.position.x + sideBufferSpawnDistance, rightWall.position.x - sideBufferSpawnDistance);
            randomVec.y = Random.Range(bottomWall.position.y + topBufferSpawnDistance, topWall.position.y - topBufferSpawnDistance);
           
            return randomVec;
        }

        public void DestroyPlant(GameObject flowerToDestroy)
        {
            Destroy(flowerToDestroy);                    
        }

        public void CutFlower(GameObject cutFlower)
        {
            AudioClip clip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/snip");

            AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
            

            if (Player.Instance.holdingFlower != null)
            {
                Destroy(Player.Instance.holdingFlower);               
            }

            Player.Instance.holdingFlower = cutFlower;
            cutFlower.transform.parent = Player.Instance.transform;
            cutFlower.gameObject.tag = "Untagged";
            cutFlower.GetComponent<CircleCollider2D>().enabled = false;
            cutFlower.GetComponent<FlowerScript>().enabled = false;
            FlowerList.Remove(cutFlower);

            EventManager.Instance.Fire(new AttackLocationEvent(Player.Instance.transform.position));
        }

        /// <summary>
        /// Picks a random color from the set
        /// </summary>
        /// <returns></returns>
        public HSVColor RandomColor()
        {
            return new HSVColor(Random.Range(0, 1f), 1, 1);
        }

        public void TurnInFlower(GameObject flowerToTurnIn)
        {

            flowerToTurnIn.transform.parent = null;            
            MoveTask moveTask = new MoveTask(flowerToTurnIn, flowerToTurnIn.transform.position, FlowerLocations[_numTurnIn].position, 1f);
            PulseScaleTask pulseScaleTask = new PulseScaleTask(flowerToTurnIn, 2, .5f);

            moveTask.Then(pulseScaleTask);
            Services.TaskManager.AddTask(moveTask);

            
            if (_numTurnIn == 0)
            {
                combinedColor = flowerToTurnIn.GetComponent<FlowerScript>().flowerColor;
            } else
            {
                combinedColor = combinedColor.MixWith(flowerToTurnIn.GetComponent<FlowerScript>().flowerColor);
            }

            ActionTask updateColor = new ActionTask(() => BarSpriteRenderer.color = combinedColor.RGBColor);
            pulseScaleTask.Then(updateColor);

            _numTurnIn++;

            _flowerBar.Add(flowerToTurnIn);

            if(_numTurnIn>=3)
            {
                WaitTask newwait = new WaitTask(.5f);
                ActionTask resetTask = new ActionTask(()=> ResetCutFlowers());
                updateColor.Then(newwait);
                newwait.Then(resetTask);
            }
        }

        public void ResetCutFlowers()
        {
            _numTurnIn = 0;

            for (int i = 0; i < _flowerBar.Count; i++)
            {
                Destroy(_flowerBar[i]);
            }
            
            _flowerBar.Clear();

            score += ScoreColors(targetColor, combinedColor);
            ScoreText.text = "Score: " + Mathf.RoundToInt(score);

            targetColor = RandomColor();
            targetSprite.color = targetColor.RGBColor;

            BarSpriteRenderer.color = Color.white;

           
        }

        public GameObject FindClosestFlower(Vector2 playerLoc, float minDist = 1000)
        {
            List<GameObject> flowerList = FlowerManager.Instance().FlowerList;
            GameObject closestFlower = null;

            for(int i = 0; i < flowerList.Count; i++)
            { 
                if (Vector2.Distance(transform.position, flowerList[i].transform.position) < minDist)
                {
                    minDist = Vector2.Distance(transform.position, flowerList[i].transform.position);
                    closestFlower = flowerList[i];
                }
            }
            return closestFlower;
        }

        /// <summary>
        /// Finds a random flower
        /// </summary>
        /// <returns></returns>
        public GameObject FindRandomFlower()
        {
            if (FlowerList.Count > 0)
            {
                int randomFlower = Random.Range(0, FlowerList.Count);
                return FlowerList[randomFlower];
            } else
            {
                return null;
            }

        }

        float ScoreColors(HSVColor color1, HSVColor color2)
        {

            float x = color1.h;
            float y = color2.h;

            if (Mathf.Abs(x - y) > 0.5f)
            {
                Debug.Log(Mathf.Abs(x - y));
                if (x < y)
                {
                    x += 1;
                }
                else
                {
                    y += 1;
                }
            }

            float colorDifference = Mathf.Abs(x - y);
            if (colorDifference < compairBuffer)
            {
                return (compairBuffer - colorDifference) * 200;
            }

            return 0;
        }
    }
}
